var mysql = require("mysql");
var app = require('express')();
var http = require('http').Server(app);
var io = require('socket.io')(http);
const bodyParser=require('body-parser');
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));



app.get('/', function(req, res){
  res.sendfile('testsql.html');
  //res.sendfile('/login/');
});
http.listen(4200, function(){
  console.log('listening on *:4200');
});

var con = mysql.createConnection({
  host: "localhost",
  user: "root",
  database: "test"
});



// io.on('connection', function (socket) {
//   console.log('a client connected');
//   con.query('SELECT * FROM demo',function(err,rows){
//     if(err) {throw err;}
//     else{
//       console.log('Data received from Db:\n');
//       console.log(rows);
//       socket.emit('showrows', rows);
//     }
   
//   });
// });
io.on('connection', function (socket) {
     console.log('a client connected');
app.get('/names', function (req, res) {
  con.query('SELECT * FROM demo', (err, rows) => {
    if(err) throw err;
          res.send(rows);
          console.log(rows);
          console.log('Data received from Db:\n');
          console.log(rows);
          res.json({
            message:'retrieved successfully'
          })
          //socket.emit('showrows', rows);  
  })
});

app.post('/new_name', function (req, res) {
  con.query('INSERT INTO demo (name,email) values(?,?)',[req.body.name,req.body.email], (err, rows) => {
    if(err){
      throw err;
    }
    else{
      console.log("data inserted successfully");
      console.log(rows);
      res.json({
        message:'data inserted successfully'
      })
      //socket.emit('showrows',rows);
    }    
  })
});

app.delete('/delname', function (req, res) {
  con.query('DELETE from demo WHERE name=?',[req.body.name], (err, rows) => {
    if(err){
      throw err;
    }
    else{
      console.log("data deleted successfully");
      console.log(rows);
      res.json({
        message:'data deleted successfully'
      })
      //socket.emit('showrows',rows);
    }    
  })
});

app.put('/update',function(req,res){
  con.query('UPDATE demo set name=? WHERE email=?',[req.body.name,req.body.email],(err,rows)=>{
    if(err){
      throw err;
    }
    else{
      console.log('updated  succesfully');
      console.log(rows);
      res.json({
        message:'data updated successfully'
      })
    }
  })
})
});